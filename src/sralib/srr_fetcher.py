#-------------------------------------------------------------------------------
#  \file srr_fetcher.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.5
#  description srr_fetcher.py handles the NCBI EDirect requests for SRA reads.
#-------------------------------------------------------------------------------
import os
import sys

sys.path.insert(1, os.path.join(sys.path[0], '../include/blib/ncbi/src'))
import edirect.callimachus.ncbi_callimachus
import edirect.efetch.efetcher
from . import search_result_analyzer
from . import run_info_parser

class SrrFetcher(edirect.callimachus.ncbi_callimachus.NcbiCallimachus):

  def __init__(self, email, tool='SraCallimachus'):
    super().__init__(email, tool)
    self.search_options = {'db' : 'sra', 'rettype':None}
    self.efetch = edirect.efetch.efetcher.Efetcher(tool, email)

  def search(self, search_term):
    r = search_result_analyzer.SearchResultAnalyzer()
    self.esearch.search(search_term, self.search_options, r)
    return r.result

  def fetch(self, search_result):
    s = run_info_parser.SraRuninfoParser()
    fetch_options = {'db' : 'sra', 'retmode': 'xml', 'rettype':'runinfo'}
    self.efetch.fetch_history_server(search_result, options=fetch_options, analyzer=s)
    return s.sramanager
