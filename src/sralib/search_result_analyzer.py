#-------------------------------------------------------------------------------
#  \file run_info_parser.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.5
#  \description The Search results anayzer for SRA requests.
#-------------------------------------------------------------------------------
import os
import sys

sys.path.insert(1, os.path.join(sys.path[0], '../include/blib/ncbi/src'))
import edirect.edbase.edanalyzer
import edirect.edbase.edresult

class SearchResultAnalyzer(edirect.edbase.edanalyzer.EdAnalyzer):

  def __init__(self):
    super().__init__()
    self.result = edirect.edbase.edresult.EdResult()

  def analyze_result(self, request):
    self.result.count = request.response['esearchresult']['count']
    self.result.webenv = request.response['esearchresult']['webenv']
    self.result.querykey = request.response['esearchresult']['querykey']
