#!/usr/bin/python3
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  \file delimitsrr.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.6
#  \description delimitsrr.py fetches SRA runs for a specific search term. The
#               results can be sorted and limited.
#  \example "Glires[ORGN] NOT (Mus musculus OR Rattus norvegicus OR Oryctolagus cuniculus OR Cavia porcellus OR Mesocricetus auratus)"
#-------------------------------------------------------------------------------
import argparse

from sralib import srr_fetcher

def main():
  ap = argparse.ArgumentParser(description='Delimitsrr, filter and sort SRA reads by organism')
  ap.add_argument('-e','--email', type=str,  help='Your email address', required=True)
  ap.add_argument('-t','--term', type=str, help='NCBI search term')
  ap.add_argument('-s','--sort-column', type=str, default='bases',
                  help='Sort SRR by: bases : number of bases (default); spots : number of sequenced spots; avglen : average read length; fsize: SRR sice in MB')
  ap.add_argument('-l', '--limit', type=int, default=0,
                  help = 'Limit to LIMIT number of SRRs, descending. Negative LIMIT limits ascending. LIMIT 0 does not limit (default)')
  ap.add_argument('--apikey', type=str,  help='NCBI Api Key')
  args = ap.parse_args()
  f = srr_fetcher.SrrFetcher(args.email)
  results = f.search( args.term)
  sramgr = f.fetch(results)
  sramgr.get_runs_by_organims(sortby=args.sort_column, limit=args.limit)
  return 0

if __name__ == '__main__':
  main()
